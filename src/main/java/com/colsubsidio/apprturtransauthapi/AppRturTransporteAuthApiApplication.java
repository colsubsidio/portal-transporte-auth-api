package com.colsubsidio.apprturtransauthapi;

import com.colsubsidio.apprturtransauthapi.dao.UsersDao;
import com.colsubsidio.apprturtransauthapi.model.documents.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
public class AppRturTransporteAuthApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppRturTransporteAuthApiApplication.class, args);
	}

}
