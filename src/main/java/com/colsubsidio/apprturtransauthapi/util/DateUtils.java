package com.colsubsidio.apprturtransauthapi.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.springframework.stereotype.Component;

@Component
public class DateUtils {
	
    public Date getDate(){
        return new Date();
    }
    
    public String getDateString(String formatReturn) {
    	try {
    		SimpleDateFormat sdf = new SimpleDateFormat(formatReturn);
        	return sdf.format(getDate());
    	}catch (Exception e) {
    		StringBuilder error = new StringBuilder();
			error.append(DateUtils.class.getName());
			error.append(";");
			error.append("formatDate");
			error.append(";");
			error.append(e.getMessage());
			return "";
		}
    }
    
    /*"yyyy-MM-dd":"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"*/
    public String formatDate(String format,String dateTxt,String formatReturn) {
		try {
			Date date = new SimpleDateFormat(format, Locale.ENGLISH).parse(dateTxt);
			SimpleDateFormat sdf = new SimpleDateFormat(formatReturn);
			return sdf.format(date);
		} catch (Exception e) {
			StringBuilder error = new StringBuilder();
			error.append(DateUtils.class.getName());
			error.append(";");
			error.append("formatDate");
			error.append(";");
			error.append(e.getMessage());
			return "";
		}
	}
    
    public String addSubtractMontsDate(int months,String formatReturn){
		try {
			Date date = addSubtractMontsDate(months);
			SimpleDateFormat sdf = new SimpleDateFormat(formatReturn);
			return sdf.format(date);
		} catch (Exception e) {
			StringBuilder error = new StringBuilder();
			error.append(DateUtils.class.getName());
			error.append(";");
			error.append("addSubtractDaysDate");
			error.append(";");
			error.append(e.getMessage());
			return "";
		}
    }
    
    public Date addSubtractMontsDate(int months){
		try {
			Calendar calendar = Calendar.getInstance();
	    	calendar.setTime(getDate());
	    	calendar.add(Calendar.MONTH, -months);
	    	return calendar.getTime();
		} catch (Exception e) {
			StringBuilder error = new StringBuilder();
			error.append(DateUtils.class.getName());
			error.append(";");
			error.append("addSubtractMontsDate");
			error.append(";");
			error.append(e.getMessage());
			return null;
		}
    }
    
    public String addSubtractHoursDate(int hours,String formatReturn){
		try {
			Date date = addSubtractHoursDate(hours);
			SimpleDateFormat sdf = new SimpleDateFormat(formatReturn);
			return sdf.format(date);
		} catch (Exception e) {
			StringBuilder error = new StringBuilder();
			error.append(DateUtils.class.getName());
			error.append(";");
			error.append("addSubtractHoursDate");
			error.append(";");
			error.append(e.getMessage());
			return "";
		}
    }
    
    public Date addSubtractHoursDate(int hours){
		try {
			Calendar calendar = Calendar.getInstance();
	    	calendar.setTime(getDate());
	    	calendar.add(Calendar.HOUR, -hours);
	    	return calendar.getTime();
		} catch (Exception e) {
			StringBuilder error = new StringBuilder();
			error.append(DateUtils.class.getName());
			error.append(";");
			error.append("addSubtractHoursDate");
			error.append(";");
			error.append(e.getMessage());
			return null;
		}
    }
    
    
    
    
   
}

