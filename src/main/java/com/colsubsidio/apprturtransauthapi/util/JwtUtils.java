package com.colsubsidio.apprturtransauthapi.util;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtUtils {

	@Value("${jwt.secret-word}")
	private String jwtSecretWord;
	@Value("${jwt.time-expiration}")
	private String jwtTimeExpiration;
	@Value("${spring.application.name}")
	private String appName;

	public String getJWTToken(String username,List<String> groups,Map<String,Object> claims) {
		String roles = "";
		for (String group : groups) {
			roles = roles+group+",";
		}
		roles = roles.length()>0? roles.substring(0, roles.length()-1):"";
		int timeExp = Integer.parseInt(jwtTimeExpiration) * 60000;
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList(roles);
		String token = Jwts
				.builder()
				.setId(appName)
				.setSubject(username)
				.addClaims(claims)
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + timeExp))
				.signWith(SignatureAlgorithm.HS512,
						jwtSecretWord.getBytes()).compact();

		return "Bearer " + token;
	}
}