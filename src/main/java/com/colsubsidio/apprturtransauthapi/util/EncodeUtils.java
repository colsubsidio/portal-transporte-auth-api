package com.colsubsidio.apprturtransauthapi.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class EncodeUtils {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public String encodePass(String value) {
		return passwordEncoder.encode(value);
	}
	
	public boolean comparePass(String passText,String passEncode) {
		return passwordEncoder.matches(passText, passEncode);
	}
}
