package com.colsubsidio.apprturtransauthapi.dto;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GeneralResponse<T> {
	private String code;
	private T body;
	private boolean success;
	private String message;
}
