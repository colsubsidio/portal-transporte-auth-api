package com.colsubsidio.apprturtransauthapi.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResAuthUser {
	
	private String username;
	private String token;
}
