package com.colsubsidio.apprturtransauthapi.dto.apigee;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ReqAuthActiveDirectoryDTO {
	private String username;
	private String password;
}
