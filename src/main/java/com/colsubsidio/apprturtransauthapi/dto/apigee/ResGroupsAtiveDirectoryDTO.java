package com.colsubsidio.apprturtransauthapi.dto.apigee;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
@Builder
public class ResGroupsAtiveDirectoryDTO {
	
	private List<Resultado> resultado;
    private List<ObtenerGrupos> obtenerGrupos;
	
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    @Data
    @Builder
	public static class ObtenerGrupos{
		
		private String descripcion;
	    private InformacionPersonal informacionPersonal;
	    private Directorio directorio;
	    private String tiempoBloqueo;
	    private String ultimoGrupo;
	    private String fechaCreacion;
	    private Usuario usuario;
	    private List<Grupos> grupos;

	}
	
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    @Data
    @Builder
	public static class Grupos{
		
		private String descripcion;
	    private String tipo;
	    private String completo;
	    private String distinguido;
	}
	
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    @Data
    @Builder
	public static class InformacionPersonal{
		
		private Nombre nombre;
	    private String correoElectronico;
	}
	
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    @Data
    @Builder
	public static class Nombre{
		
		private String completo;
	}
	
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    @Data
    @Builder
	public static class Usuario{
		
		private String principal;
	    private String cuenta;
	    private String nombre;
	}
	
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    @Data
    @Builder
	public static class Directorio{
		
		private String completo;
	    private String apellido;
	    private String distinguido;
	    private String nombre;
	}
	
    @AllArgsConstructor
    @NoArgsConstructor
    @ToString
    @Data
    @Builder
	public static class Resultado{
		
		private String descripcion;
	    private String codigo;
	}
}
