package com.colsubsidio.apprturtransauthapi.dto.apigee;

import lombok.Data;

@Data
public class ResAuthActiveDirectoryDTO {
	private boolean ok;
	private String message;
}
