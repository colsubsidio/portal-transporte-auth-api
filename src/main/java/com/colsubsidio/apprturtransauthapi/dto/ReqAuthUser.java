package com.colsubsidio.apprturtransauthapi.dto;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ReqAuthUser {
	
	private String username;
	private String password;
}
