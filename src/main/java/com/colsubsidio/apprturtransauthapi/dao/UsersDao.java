package com.colsubsidio.apprturtransauthapi.dao;

import com.colsubsidio.apprturtransauthapi.model.documents.User;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface UsersDao extends MongoRepository<User, String> {
    User findByUsuario(String username);
}
