package com.colsubsidio.apprturtransauthapi.constants;

public class TelemetryConstants {
	
	public static final String INFO = "info";
	public static final String ERROR = "error";
	
	public static final String TAG_TELEMETRY = "telemetry";
}
