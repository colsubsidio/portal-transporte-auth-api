package com.colsubsidio.apprturtransauthapi.model.documents;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "user")
public class User {
    @Id
    private String id;

    private String partitionKey;
    private String rowKey;
    private String usuario;
    private String clave;
    private String nombreCompleto;
    private String perfil;
}
