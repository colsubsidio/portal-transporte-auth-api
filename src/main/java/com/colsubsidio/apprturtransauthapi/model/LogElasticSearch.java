package com.colsubsidio.apprturtransauthapi.model;

import lombok.Data;

@Data
public class LogElasticSearch {
    String app;
    String version;
    String index;
    String type;
    String eventDate;
    int typeStatusCode;
    Object message;
    String spanIdString;
    String localRootIdString;
    String parentIdString;
    String traceIdString;


}


