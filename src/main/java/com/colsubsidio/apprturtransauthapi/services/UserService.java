package com.colsubsidio.apprturtransauthapi.services;

import com.colsubsidio.apprturtransauthapi.dao.UsersDao;
import com.colsubsidio.apprturtransauthapi.model.documents.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserService {

    @Autowired
    private UsersDao userDao;
    
    public User findByUser(String userName) {
        return this.userDao.findByUsuario(userName);
    }

    public List<User> findAll() {
        return this.userDao.findAll();
    }
}
