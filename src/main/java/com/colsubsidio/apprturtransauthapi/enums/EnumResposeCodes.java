package com.colsubsidio.apprturtransauthapi.enums;

import lombok.Getter;

@Getter
public enum EnumResposeCodes {
	
	SUCCESS("200","OK"),
	INCORRECT_CREDENTIALS("401","Usuario o contraseña incorrectas"),
	ERROR("500","Error en el sistema"),
	NA("","");
	
	private String code;
	private String description;
	
	private EnumResposeCodes(String code,String description) {
		this.code = code;
		this.description = description;
	}
	
	public static EnumResposeCodes getByCode(String code) {
		for (EnumResposeCodes e : values()) {
			if (e.code.equals(code))
				return e;
		}
		return NA;
	}
	
	public static String getByCode(EnumResposeCodes stateReserveTypeEnum) {
		for (EnumResposeCodes e : values()) {
			if (e == stateReserveTypeEnum)
				return e.getCode();
		}
		return NA.getCode();
	}
}
