package com.colsubsidio.apprturtransauthapi.enums;

import lombok.Getter;

@Getter
public enum EnumProfilesUser {
	
	ADMIN("1","Administrador","ROLE_ADMIN"),
	TRANSPORTADOR("2","Tranportador","ROLE_TRANSPORT"),
	BUSINESS("3","Empresa","ROLE_COMPANY"),
	NA("0","NA","NA");
	
	private String code;
	private String description;
	private String role;
	
	private EnumProfilesUser(String code,String description,String role) {
		this.code = code;
		this.description = description;
		this.role = role;
	}
	
	public static EnumProfilesUser getByCode(String code) {
		for (EnumProfilesUser e : values()) {
			if (e.code.equals(code))
				return e;
		}
		return NA;
	}
	
	public static String getByCode(EnumProfilesUser stateReserveTypeEnum) {
		for (EnumProfilesUser e : values()) {
			if (e == stateReserveTypeEnum)
				return e.getCode();
		}
		return NA.getCode();
	}
}
