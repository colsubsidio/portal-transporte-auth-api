package com.colsubsidio.apprturtransauthapi.business;

import com.colsubsidio.apprturtransauthapi.dto.GeneralResponse;
import com.colsubsidio.apprturtransauthapi.dto.ReqAuthUser;
import com.colsubsidio.apprturtransauthapi.dto.ResAuthUser;
import com.colsubsidio.apprturtransauthapi.enums.EnumProfilesUser;
import com.colsubsidio.apprturtransauthapi.enums.EnumResposeCodes;
import com.colsubsidio.apprturtransauthapi.model.documents.User;
import com.colsubsidio.apprturtransauthapi.services.UserService;
import com.colsubsidio.apprturtransauthapi.util.EncodeUtils;
import com.colsubsidio.apprturtransauthapi.util.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ControllerBusiness {
	
	@Autowired
	private JwtUtils jwtUtils;
	@Autowired
	private EncodeUtils encodeUtils;
	@Autowired
	private UserService userService;
	
	public GeneralResponse<ResAuthUser> authUser(ReqAuthUser reqAuthUser){
		GeneralResponse<ResAuthUser> response = GeneralResponse.<ResAuthUser>builder().build();
		boolean passUserIsCorrect;
		List<String> roles = new ArrayList<>();
		Map<String,Object> claims = new HashMap<>();
		User user = this.userService.findByUser(reqAuthUser.getUsername());

		if(user != null) {
			roles.add(EnumProfilesUser.getByCode(user.getPerfil()).getRole());
			passUserIsCorrect = encodeUtils.comparePass(reqAuthUser.getPassword(), user.getClave());

			String nameUser = user.getNombreCompleto();
			if(passUserIsCorrect) {
				claims.put("name", nameUser);
				String token = jwtUtils.getJWTToken(reqAuthUser.getUsername(),roles,claims);
				response.setBody(ResAuthUser.builder()
						.token(token)
						.username(reqAuthUser.getUsername())
						.build());
				response.setCode(EnumResposeCodes.SUCCESS.getCode());
				response.setMessage(EnumResposeCodes.SUCCESS.getDescription());
				response.setSuccess(true);
			}else {
				response.setCode(EnumResposeCodes.INCORRECT_CREDENTIALS.getCode());
				response.setMessage(EnumResposeCodes.INCORRECT_CREDENTIALS.getDescription());
				response.setSuccess(false);
			}
		}

		
		return response;
		
	}
}
