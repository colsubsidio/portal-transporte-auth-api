package com.colsubsidio.apprturtransauthapi.api;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.colsubsidio.apprturtransauthapi.business.ControllerBusiness;
import com.colsubsidio.apprturtransauthapi.dto.GeneralResponse;
import com.colsubsidio.apprturtransauthapi.dto.ReqAuthUser;
import com.colsubsidio.apprturtransauthapi.dto.ResAuthUser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@ApiResponses(value = {
		@ApiResponse(code = 200, message = "Accion exitosa"),
		@ApiResponse(code = 500, message = "Internal Server Error")
})
@Api(value = "app-rtur-transporte-auth-api ApiController", description = "Servicio Integration Manager",  tags = {"",""})
public class ApiController {

	@Autowired
	private ControllerBusiness controllerBusiness;

	@ApiOperation(value = "Autenticación de usuarios para el portal transportadores")
	@PostMapping(path = "usuario/auth", produces = "application/json")
	public ResponseEntity<GeneralResponse<ResAuthUser>> authActiveDirectory(@RequestBody ReqAuthUser reqAuthUser) {
		return new ResponseEntity<>(controllerBusiness.authUser(reqAuthUser), HttpStatus.OK);
	}
}
