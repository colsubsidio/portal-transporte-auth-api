	package com.colsubsidio.apprturtransauthapi.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.colsubsidio.apprturtransauthapi.security.JWTAuthorizationFilter;

	@Configuration
	public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable()
					.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
					.authorizeRequests()
					.antMatchers(HttpMethod.POST, "/usuario/auth/**").permitAll()
					.antMatchers(HttpMethod.GET, "/swagger-ui.html").permitAll()
					.antMatchers(HttpMethod.GET, "/swagger-ui.html/**").permitAll()
					.antMatchers(HttpMethod.GET, "/swagger-resources/**").permitAll()
					.antMatchers(HttpMethod.GET, "/configuration/**").permitAll()
					.antMatchers(HttpMethod.GET, "/configuration/ui").permitAll()
					.antMatchers(HttpMethod.GET, "/v2/api-docs").permitAll()
					.antMatchers(HttpMethod.GET, "/webjars/**").permitAll()
					.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
					.anyRequest().authenticated();
		}

		@Bean
		public PasswordEncoder encoder() {
			return new BCryptPasswordEncoder();
		}
	}
