package com.colsubsidio.apprturtransauthapi.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
public class SwaggerConfig {


	@Bean
	public Docket apiDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.any())
				.apis(RequestHandlerSelectors.basePackage("com.colsubsidio.apprturtransauthapi"))
				.paths(PathSelectors.any())
				.build().apiInfo(apiEndPointsInfo());
	}

	  
	   private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("app-rtur-transporte-auth-api")
            .description("Micro Servicio para la autenticación de usuarios al portal de transportadores")
            .contact(new Contact("Jonathann Ladino", "www.colsubsidio.com", "jonathann.ladino@ingeneo.com.co"))
            .license("Colsubsidio")
            .licenseUrl("www.colsubsidio.com")
            .version("v1.0")
            .build();
    }
	   
	   
	   @Bean
		public CorsFilter corsFilter() {
			final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
			final CorsConfiguration config = new CorsConfiguration();
			config.addAllowedOrigin("*");
			config.addAllowedHeader("*");
			config.addAllowedMethod("OPTIONS");
			config.addAllowedMethod("HEAD");
			config.addAllowedMethod("GET");
			config.addAllowedMethod("PUT");
			config.addAllowedMethod("POST");
			config.addAllowedMethod("DELETE");
			config.addAllowedMethod("PATCH");
	        config.setAllowCredentials(true);
	        source.registerCorsConfiguration("/**", config);
			return new CorsFilter(source);
		}
	
	

}

